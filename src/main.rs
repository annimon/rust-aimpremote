extern crate aimpremote;

use aimpremote::{State};

fn main() {
    println!("AIMP Version: {} ({})",
        aimpremote::version_str().unwrap_or("unknown".to_string()),
        aimpremote::version().unwrap_or(0));
    println!("Position: {} / {}",
        aimpremote::get_position().unwrap_or(0),
        aimpremote::get_duration().unwrap_or(0));
    println!("State: {:?}", aimpremote::get_state().unwrap_or(State::Stopped));
    println!("Volume: {}", aimpremote::get_volume().unwrap_or(0));
    println!("Mute: {}", aimpremote::is_mute().unwrap_or(false));
    println!("Repeat: {}", aimpremote::is_repeat().unwrap_or(false));
    println!("Shuffle: {}", aimpremote::is_shuffle().unwrap_or(false));
    println!("Radio capture: {}", aimpremote::is_radio_capture().unwrap_or(false));
    println!("Fullscreen visualisation: {}", aimpremote::is_visualization_fullscreen().unwrap_or(false));

    match aimpremote::aimp_track_info() {
        Some(track_info) => {
            println!("{:?}", track_info);
            println!("Artist: {}", track_info.artist);
            println!("Album: {}", track_info.album);
            println!("Title: {}", track_info.title);
            println!("Genre: {}", track_info.genre);
            println!("Filename: {}", track_info.filename);
            println!("Date: {}", track_info.date);
        },
        None => println!("No info")
    }
}